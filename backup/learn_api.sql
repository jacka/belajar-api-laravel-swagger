-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Mar 2020 pada 18.22
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `learn_api`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `colleagues`
--

CREATE TABLE `colleagues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `speciality` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `colleagues`
--

INSERT INTO `colleagues` (`id`, `first_name`, `last_name`, `gender`, `email`, `speciality`, `phone_number`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Russel', 'Welch', 'male', 'amayert@yahoo.com', 'Animation', '507.574.6714', '1266 Berneice Garden\nLeschville, HI 22382-4605', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(2, 'Kyla', 'Rogahn', 'female', 'lswift@yost.com', 'Photography', '570.334.4641 x85199', '3907 Josiah Terrace\nCaspermouth, OK 92662-4197', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(3, 'Elbert', 'Graham', 'male', 'beahan.javonte@funk.com', 'Design', '1-423-731-4281 x05839', '12850 Gusikowski Avenue\nWyattburgh, OR 36682', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(4, 'Georgiana', 'Schaden', 'female', 'yfritsch@weissnat.com', 'Animation', '+1-613-507-0290', '23790 Alexa Crossing\nSouth Nayeli, GA 53074-9982', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(5, 'Patience', 'Reichert', 'female', 'lakin.kaylee@hotmail.com', 'Design', '(562) 830-5511 x77802', '88291 Williamson View Suite 689\nPort Melyssaton, MD 93883-4471', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(6, 'Efrain', 'Renner', 'male', 'sporer.hilton@orn.com', 'Design', '1-584-380-7678', '5773 Armstrong Fields\nTremblaymouth, MT 84114-4280', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(7, 'Damian', 'Krajcik', 'male', 'schimmel.adrianna@koepp.com', 'Photography', '+1-986-596-7762', '6861 Sigmund Green\nMariamfort, ID 17940-7472', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(8, 'Corine', 'Rempel', 'female', 'sherman.beatty@yahoo.com', 'Animation', '(492) 394-5824 x2663', '802 Auer Extension\nAshleighland, SC 51056-2516', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(9, 'Chester', 'Pacocha', 'male', 'emely.howell@batz.com', 'Design', '1-591-909-5280 x0941', '4110 Amara Brook\nNew Daryl, MI 40252', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(10, 'Lucious', 'Fadel', 'male', 'keebler.kathlyn@runte.com', 'Animation', '(928) 237-3856 x2558', '98128 Kayla Terrace Apt. 433\nWunschmouth, CT 62037-0010', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(11, 'Noah', 'Mraz', 'male', 'ideckow@yahoo.com', 'Photography', '+1.301.216.6199', '4861 Stark Keys Suite 863\nRoycehaven, AL 11237-2317', '2020-03-16 03:51:29', '2020-03-16 03:51:29'),
(12, 'Misael', 'Franecki', 'male', 'barney.veum@hotmail.com', 'Photography', '240-265-1413', '3557 Autumn Lights\nLake Emmanuelle, UT 60397', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(13, 'Angelica', 'Lind', 'female', 'christy73@gmail.com', 'Animation', '773.417.3641', '1647 Alanis Neck Apt. 582\nLake Shana, MD 13605-1996', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(14, 'Kirsten', 'Lowe', 'female', 'lemke.scot@kilback.biz', 'Photography', '743.918.1078', '909 Schumm Garden Apt. 055\nDanialland, GA 98510-5019', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(15, 'Shanon', 'Raynor', 'female', 'qdibbert@yahoo.com', 'Photography', '1-296-721-3890 x750', '78989 Nick Turnpike\nNew Martina, HI 05596', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(16, 'Edward', 'Hoppe', 'male', 'mraynor@gmail.com', 'Animation', '445-449-2779', '944 Kristin Wall\nWest Marcelland, AR 06166', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(17, 'Emmet', 'Ledner', 'male', 'rgrant@yahoo.com', 'Photography', '(239) 841-0762 x903', '998 Price Junction Suite 180\nSouth Osborneside, AL 30561', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(18, 'Leland', 'Feeney', 'male', 'orn.vergie@yahoo.com', 'Photography', '656.362.6319', '30901 Makayla Walks\nHerminafort, OH 00711-6126', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(19, 'Florian', 'Schimmel', 'male', 'roob.myra@schuppe.biz', 'Programming', '+1-915-771-0351', '942 Bahringer Crossroad\nMartinebury, AZ 22381', '2020-03-16 03:51:30', '2020-03-16 03:51:30'),
(20, 'Ruben', 'Hilpert', 'male', 'iwalker@gaylord.net', 'Animation', '1-381-804-9629 x13756', '342 Narciso Harbors\nKarelleville, NJ 94463-6586', '2020-03-16 03:51:30', '2020-03-16 03:51:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_03_16_102904_create_colleagues_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `colleagues`
--
ALTER TABLE `colleagues`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `colleagues_email_unique` (`email`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `colleagues`
--
ALTER TABLE `colleagues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
