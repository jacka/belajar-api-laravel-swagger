<?php

namespace App\Http\Controllers\Api;

use App\Colleague;
use App\Http\Controllers\Controller;
use App\Http\Resources\ColleagueCollection;
use App\Http\Resources\ColleagueItem;
use Illuminate\Http\Request;

class ColleagueController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/colleagues",
     *      tags={"Colleague"},
     *      summary="Get All colleagues",
     *      @SWG\Response(response=200,description="successful operation"),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found")
     * )
     *
     */ 
    public function index()
    {
        return new ColleagueCollection(Colleague::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

      /**
     * @SWG\Post(
     *      path="/colleagues",
     *      tags={"Colleague"},
     *      summary="Add colleagues",
     *      @SWG\Parameter(name="first_name", description="Banner Title", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="last_name",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="email",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="gender",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="address",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="phone_number",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="speciality",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Response(response=200,description="success"),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=403, description="Forbidden"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     *
     */  
    public function store(Request $request)
    {
        $this->validate($request, Colleague::rules(false));

        if (!Colleague::create($request->all())) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'OK',
                'code' => 200,
            ];
        }
    }

     /**
     * @SWG\Get(
     *      path="/colleagues/{id}",
     *      tags={"Colleague"},
     *      summary="Get Colleague by id",
     *      @SWG\Parameter(name="id", required=true, type="string", in="path"),
     *      @SWG\Response(response=200,description="success")
     * )
     *
     */
    public function show(Colleague $colleague)
    {
        return new ColleagueItem($colleague);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colleague  $colleague
     * @return \Illuminate\Http\Response
     */
    public function edit(Colleague $colleague)
    {
        //
    }

     /**
     * @SWG\Patch(
     *      path="/colleagues/{id}",
     *      tags={"Colleague"},
     *      summary="Edit Colleague",
     *      @SWG\Parameter(name="id", description="Banner Id",  type="number", required=true, in="path"),
     *      @SWG\Parameter(name="first_name", description="Banner Title", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="last_name",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="email",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="gender",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="address",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="phone_number",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Parameter(name="speciality",  description="Banner Link", required=true, type="string", in="formData"),
     *      @SWG\Response(response=200,description="success"),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=403, description="Forbidden"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     *
     */ 
    public function update(Request $request, Colleague $colleague)
    {
        $this->validate($request, Colleague::rules(true, $colleague->id));

        if (!$colleague->update($request->all())) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'OK',
                'code' => 201,
            ];
        }
    }

    /**
     * @SWG\Delete(
     *      path="/colleagues/{id}",
     *      tags={"Colleague"},
     *      summary="Delete Colleague",
     *      @SWG\Parameter(name="id", description="Banner Id",  type="number", required=true, in="path"),
     *      @SWG\Response(response=200,description="success"),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=403, description="Forbidden"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     *
     */   
    public function destroy(Colleague $colleague)
    {
        if ($colleague->delete()) {
            return [
                'message' => 'OK',
                'code' => 204,
            ];
        } else {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        }
    }
    
}
